# tool
extends Node2D

export var radius = float(25)
export var color = Color(0,0,0)
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time 1the node is added to the scene.
	# Initialization here
	pass
	
func _draw():
	print("Pos:", global_position)
	draw_circle(global_position, radius, color)
	update()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass